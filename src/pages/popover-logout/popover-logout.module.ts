import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoverLogoutPage } from './popover-logout';

@NgModule({
  declarations: [
    PopoverLogoutPage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverLogoutPage),
  ],
})
export class PopoverLogoutPageModule {}
